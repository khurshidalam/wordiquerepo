<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<!-- start footer-wrapper -->
<div class="footer-wrapper">
<div class="wrapper-in">
<div class="common-wrap">
<div class="linkbox">
<div class="linkmenu">
<ul>
<li><a href="<?php echo home_url('/'); ?>"><i class="fa fa-angle-right"></i> Home</a></li>
<li><a href="<?php echo get_permalink(48); ?>"><i class="fa fa-angle-right"></i> About Us  </a></li>
<li><a href="<?php echo get_permalink(56); ?>"><i class="fa fa-angle-right"></i> Member Resources</a></li>
</ul>
</div>
</div>
<div class="linkbox">
<div class="linkmenu">
<ul>
<li><a href="<?php echo get_permalink(74); ?>"><i class="fa fa-angle-right"></i> FAQ </a></li>
<li><a href="<?php echo get_permalink(63); ?>"><i class="fa fa-angle-right"></i> Sign Up </a></li>
<li><a href="<?php echo get_permalink(82); ?>"><i class="fa fa-angle-right"></i> Contact Us</a></li>
</ul>
</div>
</div>
<div class="subscribe">
<div class="footer-heading">Subscribe our Newsletter</div>
<div class="subscribebox">
<input name="" type="text" placeholder="Enter your email address">
<input name="" type="submit" value="Subscribe Now">

</div>
</div>
<div class="social">
<div class="footer-heading">STAY CONNECTED</div>
  <div class="foot_socl_icon_area">
          <ul>
            <li><i class="fa fa-facebook fb"></i></li>
            <li><i class="fa fa-twitter twitter"></i></li>
            <li><i class="fa fa-google-plus google"></i></li>
          </ul>
        </div>
</div>
<div class="footer-copyright">
<div class="footerlogo"><a href=""><img src="<?php bloginfo('template_directory');?>/images/footer-logo.png" alt="" /></a></div>
<span>|</span> Copyright &copy; 2014 Wordique. All rights reserved. <span class="termtxt"><a href="">Terms and Conditions</a></span>
<?php if(is_page('home')): ?>
<div class="designtext"><a href="http://www.b3net.com/custom-web-application-development.html" target="_blank">Website design by: B3NET.com</a></div>
<?php endif; ?>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<!-- end footer-wrapper -->

</div>
<!-- end main-wrapper -->

<script src="<?php bloginfo('template_directory'); ?>/js/accodian.js"></script>
<?php wp_footer(); ?>

</body>
</html>