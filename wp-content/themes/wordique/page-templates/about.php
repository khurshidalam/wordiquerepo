<?php
/**
 * Template Name: About Us
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="">Home</a></li>
<li>/</li>
<li>About Us</li>
</ul>
</div>
<div class="about-heading"><span>about us</span></div>
</div>
</div>
<div class="about-uswrap-mid">
<div class="wrapper-in">
<div class="about-common-wrap">
<div class="about-left">
<div class="abouttxt">We are <span>Wordique</span></div>
<div class="abouttxt2">
<p>Here at Wordique, we want every writer to prosper and believe each one has a distinctive voice and talent. We realize that all writers have different backgrounds that form their composition style, medium, preferred genre, unique voice, as well as their future aspirations for their writing careers. Our site is profoundly different from our competitors because we don't set your rates, control your access to projects by judging your skills and labeling you, or make it hard to find connections.  We put the control in your hands to create your writing business, and of course, we are here to help if you need it.</p>
<p>Additionally, we recognize the demand for quality material. Many businesses and individuals require accessibility to excellent writing content for business or creative use, but do not know where to turn to find talented reliable writers. We have taken the guesswork out of your need for content and made it easy to filter through and find the perfect creative match. If you need a professional blogger, ghostwriter, or anything in-between, we have the writer for you! We have the best writers readily available, which makes looking for an excellent writer headache-free and cost effective. </p>
<p><strong>We want to welcome you to Wordique.com and if you have any questions don't hesitate to contact us.</strong> </p>
</div>
</div>
</div>
</div>
</div>
<div class="about-uswrap-lower">
<div class="wrapper-in">
<div class="about-common-wrap">
<div class="about-right">
<div class="abouttxt">Wy <span>Wordique</span> ?</div>
<div class="abouttxt2">
<p>Wordique is a unique networking website that bridges the gap for writers to find paid work from businesses or individuals searching for writers for various projects. Our goal is to create a flourishing creative environment for our clients.</p>
</div>
<div class="abouttxt3">For Writers</div>
<div class="about-righttxt">
<ul>
<li><i class="fa fa-angle-right"></i> We only allow United States citizens to post jobs giving you a lot more security than other sites.</li>
<li><i class="fa fa-angle-right"></i> We provide customizable profiles to showcase your talent.</li>
<li><i class="fa fa-angle-right"></i> We have a safe and secure payment method.</li>
<li><i class="fa fa-angle-right"></i> We take the lowest commission in the industry. </li>
<li><i class="fa fa-angle-right"></i> You set your own prices.</li>
<li><i class="fa fa-angle-right"></i> We don't pre-label you based on your test scores. </li>
</ul>
</div>
<div class="abouttxt3">For Writers</div>
<div class="about-righttxt">
<ul>
<li><i class="fa fa-angle-right"></i> We only allow United States based writers to sign up.</li>
<li><i class="fa fa-angle-right"></i> We offer a safe payment method, and the funds are never released 
until you approve the final draft.</li>
<li><i class="fa fa-angle-right"></i> We evaluate all writers./li>
<li><i class="fa fa-angle-right"></i> We allow public feedback on all profiles, so you can get an idea of a 
writer's past performance.</li>
<li><i class="fa fa-angle-right"></i> No job is too big or small to be posted on Wordique.com. If you have 
a writing project, we have a writer!</li>
</ul>
</div>
</div>
</div>
</div>
</div>

<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="<?php echo get_permalink(4); ?>">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>