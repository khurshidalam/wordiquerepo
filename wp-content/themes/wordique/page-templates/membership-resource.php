<?php
/**
 * Template Name: Membar Resources
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="">Home</a></li>
<li>/</li>
<li>Member Resources</li>
</ul>
</div>
<div class="about-heading"><span>Member Resources</span></div>
<div class="member-resources-lft">
<div class="abouttxt2" style="border-top:3px solid #76767a;">
<p>We pride ourselves on providing the safest environment for you. If you have any concerns regarding a transaction with another user, please fill out a claim form. We take ALL claims seriously and strive to find you a quick resolution.</p>
<p>Before proceeding with a claim, make sure that you have first tried to resolve the issue with the other party and that the other party involved is aware of the issue. Also, please allow at least 72 hours for the other party to respond to your concerns. If you have done all the steps above and there is still an issue, please fill out a claim form.</p>

<div class="abouttxt3">Safety Warning</div> 

<p>For your protection, we ask that all correspondence and transactions are performed through Wordique.com.  If a member requests a money order, cashier check, or another form of payment outside of Wordique.com please forward their information to <a href="mailto:sitesecurity@wordique.com">sitesecurity@wordique.com</a></p>

<p>Any form of outside payment is against the site's rules and regulations, and will result in a permanent ban from membership and will immediately terminate all membership privileges.</p>

<p>Need help finding rates for a project contact us at <a href="mailto:pricinginfo@wordique.com">pricinginfo@wordique.com</a></p>
</div>
</div>
<div class="member-resources-rgt">
<div class="member-resources-rgtin">
<div class="member-heading"><span>File a Complain</span></div>
<div class="member-formbox">
<div class="member-formbox-txt">You are a...</div>
<div class="member-formbox-field">
<select name="allproduct" id="allproduct1" style="width:100%;" tabindex="1">
  <option value="Lorem Ipsum1">Customer</option>
  <option value="Lorem Ipsum1">Seeking a Writer</option>
</select>
<script type="text/javascript">
try {
oHandler = $("#allproduct1").msDropDown({mainCSS:'dd4'}).data("dd");
$("#ver").html($.msDropDown.version);
} catch(e) {
alert("Error: "+e.message);
}
</script> 
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Your Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Account Number</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Client Id</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Nature of Problem</div>
<div class="member-formbox-field">
<select name="allproduct" id="allproduct2" style="width:100%;" tabindex="1">
  <option value="Lorem Ipsum1">Please Select...</option>
</select>
<script type="text/javascript">
try {
oHandler = $("#allproduct2").msDropDown({mainCSS:'dd4'}).data("dd");
$("#ver").html($.msDropDown.version);
} catch(e) {
alert("Error: "+e.message);
}
</script> 
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Describe your Problem</div>
<div class="member-formbox-field">
<textarea class="member-formbox-txtarea"></textarea>
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">
<div class="member-formbox-txt1">Security Code *</div>
<div class="member-formbox-txt2"><span>*</span> = required field</div>
</div>
<div class="member-formbox-capchatxt">Help us fight spam by completing the following</div>
<div class="member-formbox-field">
<div class="member-formbox-capcha"><img src="<?php bloginfo('template_directory');?>/images/member-capture.png" alt="" /></div>
<input type="text" class="member-formbox-txtfield1" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-field">
<div class="member-formbox-submit">
<input type="submit" value="send message"/>
</div>
</div>
</div>
</div>


</div>
</div>

</div>
<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="<?php echo get_permalink(4); ?>">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>