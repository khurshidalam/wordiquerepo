<?php
/**
 * Template Name: FAQ
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="">Home</a></li>
<li>/</li>
<li>Faq</li>
</ul>
</div>
<div class="about-heading"><span>Faq</span></div>
<div class="member-resources-lft">
<div class="faq-lft">
        <input type="text" placeholder="Have a question? Ask or enter a search term here." name="">
        <input type="submit" value="Sign Up Free" name="">
      </div>


<div class="graphite demo-container">
    <ul class="accordion1" id="accordion-2">
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>

    </li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt">
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.
</li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.

    </li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt">
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.
</li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    </ul>
    </div>




</div>
<div class="member-resources-rgt">
<div class="faq-rgtbox">
 <div class="faqheading">Common Questions</div>
<div class="faqtxt">
<ul>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</li>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</li>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text ?</li>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</li>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text ?</li>
<li><i class="fa fa-check-circle"></i> Lorem Ipsum is simply dummy text ?</li>
</ul>
</div>
</div>
<div class="faq-rgtbox">
 <div class="faqheading">Contact Wordique Support</div>
<div class="faqtxt">
<ul>
<li><i class="fa fa-check-circle"></i> Post a project</li>
<li><i class="fa fa-check-circle"></i> Start a claim</li>
<li><i class="fa fa-check-circle"></i> Contact us</li>
</ul>
</div>
</div>
</div>
</div>

</div>
<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="<?php echo get_permalink(4); ?>">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>