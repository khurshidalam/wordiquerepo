<?php
/**
 * Template Name: Writer Sign Up
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>


<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="<?php echo home_url('/'); ?>">Home</a></li>
<li>/</li>
<li><a href="<?php echo get_permalink(63); ?>">Sign Up</a></li>
<li>/</li>
<li>Create a Free Freelancer Account</li>
</ul>
</div>
<div class="about-heading"><span>Sign Up</span></div>
<div class="headingtxt">Create a Free Freelancer Account</div>
<div class="client-subheading">If you already have an account with us, please login at the <span><a href="<?php echo home_url('/wp-login.php'); ?>">login page.</a></span></div>


<div class="client-signup">
   
  <?php echo do_shortcode('[user-meta-registration form="User Registration"]');?>  
<!--<form action="" method="">
<div class="client-signup-formbox">
<div class="client-signup-formboxtxt">Your personal details</div>
<div class="client-signup-formrgt">
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>First Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt">Last Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>User Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>E-mail</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>Phone</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
</div>
</div>
<div class="client-signup-formbox">
<div class="client-signup-formboxtxt">Your address</div>
<div class="client-signup-formrgt">
<div class="client-signup-formrgtin">
<div class="member-formbox-txt">Company Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>Address</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>City</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>Post Code</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>Region/sate</div>
<div class="member-formbox-field">
<select name="allproduct" id="allproduct5" style="width:100%;" tabindex="1">
  <option value="Lorem Ipsum1">Please Select</option>
</select>
<script type="text/javascript">
try {
oHandler = $("#allproduct5").msDropDown({mainCSS:'dd4'}).data("dd");
$("#ver").html($.msDropDown.version);
} catch(e) {
alert("Error: "+e.message);
}
</script> 
</div>
</div>
</div>
</div>
<div class="client-signup-formbox">
<div class="client-signup-formboxtxt">Your password</div>
<div class="client-signup-formrgt">
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>password</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt"><span>*</span>Password Confirm</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="client-signup-formrgtin">
<div class="member-formbox-txt">
<div class="member-formbox-txt1">Security Code *</div>
</div>
<div class="member-formbox-capchatxt">Help us fight spam by completing the following</div>
<div class="member-formbox-field">
<div class="member-formbox-capcha"><img src="images/member-capture.png" alt="" /></div>
<input type="text" class="member-formbox-txtfield1" />
</div>
</div>
<div class="client-signup-submit"><input type="submit" value="Get Started" /></div>
<div class="client-signup-formrgtin">
<label class="label_check1" for="checkbox-01">
<input name="image_id[1]" id="checkbox-01" value="1" type="checkbox" />
<span class="cheak-text">I have read an agree to </span> <span class="cheak-text1">Tearms and Conditions </span></label>
<div class="member-formbox-txt2"><span>*</span> = required field</div>
</div>
</div>
</div>
</form>
-->
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>