<?php
/**
 * Template Name: Contact Us
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="">Home</a></li>
<li>/</li>
<li>Contacts</li>
</ul>
</div>
<div class="about-heading"><span>Contacts </span></div>

<div class="contact-helpbox">
<div class="heading">Have any question?</div>
<div class="subheading">Feel free to contact us anytime, our experts will respond as soon as possible!</div>
<form action="" method="">
<div class="formrowbox">
<div class="formrowtext">Name</div>
<input name="name" type="text" class="member-formbox-txtfield">
</div>
<div class="formrowbox">
<div class="formrowtext">Last Name</div>
<input name="email" type="text" class="member-formbox-txtfield">
</div>
<div class="formrowbox">
<div class="formrowtext">Contact Number</div>
<input name="phone" type="text" class="member-formbox-txtfield" maxlength="10">
</div>
<div class="formrowbox">
<div class="formrowtext">E-mail</div>
<input name="email" type="text" class="member-formbox-txtfield">
</div>
<div class="formrowfullbox">
<div class="formrowtext">Message</div>
<textarea name="" class="member-formbox-txtarea"></textarea>
</div>
<div class="formrowfullbox1">
<div class="member-formbox">
<div class="member-formbox-txt">
<div class="member-formbox-txt1">Security Code *</div>
</div>
<div class="member-formbox-capchatxt">Help us fight spam by completing the following</div>
<div class="member-formbox-field">
<div class="member-formbox-capcha"><img alt="" src="<?php bloginfo('template_directory');?>/images/member-capture.png"></div>
<input type="text" class="member-formbox-txtfield1">
</div>
</div>
</div>
<div class="formrowfullbox2">
<div class="member-formbox-submit">
<input type="submit" value="send message"/>
</div>
<div class="member-formbox-txt2"><span>*</span> = required field</div>
</div>
</form>
</div>




</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>