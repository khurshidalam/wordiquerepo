<?php
/**
 * Template Name: Sign Up
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="<?php echo home_url('/'); ?>">Home</a></li>
<li>/</li>
<li>Sign Up</li>
</ul>
</div>
<div class="about-heading"><span>Sign Up</span></div>
<div class="headingtxt">First, tell us what you're looking for...</div>
<div class="signup-box">
<div class="freelancer">
<div class="signup-boxin">
<div class="signlft"><img src="<?php bloginfo('template_directory'); ?>/images/signup-lftpic.png" alt="" /></div>
<div class="signrgt">
<div class="signrgttxt">I need a freelancer</div>
<div class="signrgttxt1">Lorem ipsum dolor sit consectetur adipiscing elit, labore et dolore magna aliqua.</div>
<div class="signrgtbtn"><a href="<?php echo get_permalink(95); ?>">Sign Up</a></div>
</div>
</div>
</div>
<div class="job">
<div class="signup-boxin">
<div class="signlft"><img src="<?php bloginfo('template_directory'); ?>/images/signup-rgtpic.png" alt="" /></div>
<div class="signrgt">
<div class="signrgttxt">I need a job</div>
<div class="signrgttxt1">Lorem ipsum dolor sit consectetur adipiscing elit, labore et dolore magna aliqua.</div>
<div class="signrgtbtn"><a href="<?php echo get_permalink(93); ?>">Sign Up</a></div>
</div>
</div>
</div>
</div>



</div>
</div>
<!-- end body wrapper-->
<?php get_footer(); ?>