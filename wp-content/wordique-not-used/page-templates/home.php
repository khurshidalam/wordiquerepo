<?php
/**
 * Template Name: Home
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- start banner-wrapper -->
<div class="banner-wrapper">
<div id="owl-demo" class="owl-carousel">
<div class="item">
<div class="banner-content">
<div class="topsec">
<div class="heading-text">Lorem Ipsum dolor sit amet, consectetur adipiscing elit</div>
</div>
<div class="banner-btn"><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn.png"/></a><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn1.png"/></a></div>
</div>
<img src="<?php bloginfo('template_directory');?>/images/slide-02.jpg">
</div>
<div class="item">
<div class="banner-content">
<div class="topsec">
<div class="heading-text">Lorem Ipsum dolor sit amet, consectetur adipiscing elit</div>
</div>
<div class="banner-btn"><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn.png"/></a><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn1.png"/></a></div>
</div>
<img src="<?php bloginfo('template_directory');?>/images/slide-02.jpg">
</div>
<div class="item">
<div class="banner-content">
<div class="topsec">
<div class="heading-text">Lorem Ipsum dolor sit amet, consectetur adipiscing elit</div>
</div>
<div class="banner-btn"><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn.png"/></a><a href="#"><img src="<?php bloginfo('template_directory');?>/images/banner-btn1.png"/></a></div>
</div>
<img src="<?php bloginfo('template_directory');?>/images/slide-02.jpg">
</div>
</div>
</div>
<!-- end banner-wrapper -->

<!-- body wrapper -->
<div class="writers-section">
<div class="wrapper-in">
   <div class="writers-section-lft">
            <div class="form_drp_dwn_box">
                <select name="allproduct" id="allproduct" style="width:300px;" tabindex="1">
                  <option value="Lorem Ipsum1">WRITERS</option>
                  <option value="Lorem Ipsum1">Seeking a Writer</option>
                </select>
				<script type="text/javascript">
                try {
                oHandler = $("#allproduct").msDropDown({mainCSS:'dd3'}).data("dd");
                $("#ver").html($.msDropDown.version);
                } catch(e) {
                alert("Error: "+e.message);
                }
                </script> 
            </div>
        <input name="" type="text" placeholder="Enter your company email">
        <input name="" type="submit" value="Sign Up Free">
      </div>
      <div class="postbtn"><a href="post-a-job/">Post a Project</a></div>

</div>
</div>
<div class="feature-writer-section">
 <div class="wrapper-in">
  <div class="common-wrap">
  <div class="feature-txt">Featured Writers 
  <div class="see-more"><a href="">See more <i class="fa fa-plus"></i>
</a></div>
  </div>
  <div class="feature-writer-box">
  <ul>
  <li>
  <div class="feature-inner">
  <div class="feature-inner-left"><img src="<?php bloginfo('template_directory');?>/images/pic.jpg" alt="" /></div>
  <div class="feature-inner-right">
  <div class="feature-inner-righttxt">Olivia <span>|</span>
  <div class="feature-inner-rating"><img src="<?php bloginfo('template_directory');?>/images/rating.png" alt="" /></div>
  </div>
  <div class="feature-inner-righttxt1">Georgia, Boston</div>
  <div class="feature-inner-righttxt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</div>
  <div class="know-morebtn"><a href="">Know more</a></div>
  </div>
  </div>
  </li>
  <li>
  <div class="feature-inner">
  <div class="feature-inner-left"><img src="<?php bloginfo('template_directory');?>/images/pic1.jpg" alt="" /></div>
  <div class="feature-inner-right">
  <div class="feature-inner-righttxt">Jacob <span>|</span>
  <div class="feature-inner-rating"><img src="<?php bloginfo('template_directory');?>/images/rating.png" alt="" /></div>
  </div>
  <div class="feature-inner-righttxt1">California, Albany</div>
  <div class="feature-inner-righttxt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</div>
  <div class="know-morebtn"><a href="">Know more</a></div>
  </div>
  </div>
  </li>
  <li>
  <div class="feature-inner">
  <div class="feature-inner-left"><img src="<?php bloginfo('template_directory');?>/images/pic2.jpg" alt="" /></div>
  <div class="feature-inner-right">
  <div class="feature-inner-righttxt">Michael <span>|</span>
  <div class="feature-inner-rating"><img src="<?php bloginfo('template_directory');?>/images/rating.png" alt="" /></div>
  </div>
  <div class="feature-inner-righttxt1">Maine, Denmark</div>
  <div class="feature-inner-righttxt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</div>
  <div class="know-morebtn"><a href="">Know more</a></div>
  </div>
  </div>
  </li>
  <li>
  <div class="feature-inner">
  <div class="feature-inner-left"><img src="<?php bloginfo('template_directory');?>/images/pic3.jpg" alt="" /></div>
  <div class="feature-inner-right">
  <div class="feature-inner-righttxt">Daniel <span>|</span>
  <div class="feature-inner-rating"><img src="<?php bloginfo('template_directory');?>/images/rating.png" alt="" /></div>
  </div>
  <div class="feature-inner-righttxt1">California, Albany</div>
  <div class="feature-inner-righttxt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</div>
  <div class="know-morebtn"><a href="">Know more</a></div>
  </div>
  </div>
  </li>
  </ul>
  
  </div>
  <div class="postbtn1"><a href="">Post Your Project Today!</a></div>
  </div>
 </div>
</div>
<div class="video-section">
<div class="wrapper-in">
<div class="video-section-in"><img src="<?php bloginfo('template_directory');?>/images/video-pic.jpg" alt="" /></div>
</div>
</div>
<div class="what-people-sectionbox">
<div class="what-people-section">
<div class="wrapper-in">
<div class="common-wrap">
 <div class="what-people-heading">What are people saying about Wordique?</div>
 <div class="what-people-banner">
 <div class="flexslider">
<ul class="slides">
<li><div class="leftpic"><img src="<?php bloginfo('template_directory');?>/images/pic4.jpg" alt="" /></div>
<div class="rightsec"><div class="commapic"><img src="<?php bloginfo('template_directory');?>/images/comma.png" alt="" /></div>
<div class="commapictxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
<div class="lowertxt">Kevin Knight</div>
<div class="lowertxt1">Co-Founder, Company name</div>
</div>
</div>
</li>
<li><div class="leftpic"><img src="<?php bloginfo('template_directory');?>/images/pic4.jpg" alt="" /></div>
<div class="rightsec"><div class="commapic"><img src="<?php bloginfo('template_directory');?>/images/comma.png" alt="" /></div>
<div class="commapictxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
<div class="lowertxt">Kevin Knight</div>
<div class="lowertxt1">Co-Founder, Company name</div>
</div>
</div>
</li>
<li><div class="leftpic"><img src="<?php bloginfo('template_directory');?>/images/pic4.jpg" alt="" /></div>
<div class="rightsec"><div class="commapic"><img src="<?php bloginfo('template_directory');?>/images/comma.png" alt="" /></div>
<div class="commapictxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
<div class="lowertxt">Kevin Knight</div>
<div class="lowertxt1">Co-Founder, Company name</div>
</div>
</div>
</li>
</ul>
</div>
 </div>
</div>
</div>
</div>
</div>
<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer();?>