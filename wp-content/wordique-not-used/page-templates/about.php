<?php
/**
 * Template Name: About Us
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li>Home</li>
<li>/</li>
<li><a href="">About Us</a></li>
</ul>
</div>
<div class="about-heading"><span>about us</span></div>
</div>
</div>
<div class="about-uswrap-mid">
<div class="wrapper-in">
<div class="about-common-wrap">
<div class="about-left">
<div class="abouttxt">We are <span>Wordique</span></div>
<div class="abouttxt1">Lorem Ipsum is simply dummy text</div>
<div class="abouttxt2">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
</div>
</div>
</div>
</div>
</div>
<div class="about-uswrap-lower">
<div class="wrapper-in">
<div class="about-common-wrap">
<div class="about-right">
<div class="abouttxt">Wy <span>Wordique</span> ?</div>
<div class="abouttxt1">Lorem Ipsum is simply dummy text</div>
<div class="abouttxt2">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
</div>
<div class="about-righttxt">
<ul>
<li><i class="fa fa-angle-right"></i> Lorem Ipsum is simply dummy text of the typesetting industry.</li>
<li><i class="fa fa-angle-right"></i> Lorem Ipsum is simply dummy text of the typesetting industry.</li>
<li><i class="fa fa-angle-right"></i> Lorem Ipsum is simply dummy text of the typesetting industry.</li>
<li><i class="fa fa-angle-right"></i> Lorem Ipsum is simply dummy text of the typesetting industry.</li>
</ul>
</div>
</div>
</div>
</div>
</div>

<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->

<?php get_footer(); ?>