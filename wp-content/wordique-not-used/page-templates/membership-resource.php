<?php
/**
 * Template Name: Membar Resources
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- body wrapper -->
<div class="about-uswrap">
<div class="wrapper-in">
<div class="breadcrumbs">
<ul>
<li><a href="">Home</a></li>
<li>/</li>
<li>Member Resources</li>
</ul>
</div>
<div class="about-heading"><span>Member Resources</span></div>
<div class="member-resources-lft">
<div class="graphite demo-container">
    <ul class="accordion" id="accordion-1">
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>

    </li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt">
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.
</li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.

    </li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt">
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <span><a href="">start a claim</a></span></p>.
</li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    <li><a href="#">Lorem Ipsum is simply dummy text of the typesetting industry.</a>
    <ul>
    <li class="graphite-txt"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></li>
    
    </ul>
    </li>
    </ul>
    </div>
</div>
<div class="member-resources-rgt">
<div class="member-resources-rgtin">
<div class="member-heading"><span>File a Complain</span></div>
<div class="member-formbox">
<div class="member-formbox-txt">You are a...</div>
<div class="member-formbox-field">
<select name="allproduct" id="allproduct1" style="width:100%;" tabindex="1">
  <option value="Lorem Ipsum1">Customer</option>
  <option value="Lorem Ipsum1">Seeking a Writer</option>
</select>
<script type="text/javascript">
try {
oHandler = $("#allproduct1").msDropDown({mainCSS:'dd4'}).data("dd");
$("#ver").html($.msDropDown.version);
} catch(e) {
alert("Error: "+e.message);
}
</script> 
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Your Name</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Account Number</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Client Id</div>
<div class="member-formbox-field">
<input type="text" class="member-formbox-txtfield" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Nature of Problem</div>
<div class="member-formbox-field">
                <select name="allproduct" id="allproduct2" style="width:100%;" tabindex="1">
                  <option value="Lorem Ipsum1">Please Select...</option>
                </select>
				<script type="text/javascript">
                try {
                oHandler = $("#allproduct2").msDropDown({mainCSS:'dd4'}).data("dd");
                $("#ver").html($.msDropDown.version);
                } catch(e) {
                alert("Error: "+e.message);
                }
                </script> 
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">Describe your Problem</div>
<div class="member-formbox-field">
<textarea class="member-formbox-txtarea"></textarea>
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-txt">
<div class="member-formbox-txt1">Security Code *</div>
<div class="member-formbox-txt2"><span>*</span> = required field</div>
</div>
<div class="member-formbox-capchatxt">Help us fight spam by completing the following</div>
<div class="member-formbox-field">
<div class="member-formbox-capcha"><img src="<?php bloginfo('template_directory');?>/images/member-capture.png" alt="" /></div>
<input type="text" class="member-formbox-txtfield1" />
</div>
</div>
<div class="member-formbox">
<div class="member-formbox-field">
<div class="member-formbox-submit">
<input type="submit" value="send message"/>
</div>
</div>
</div>
</div>


</div>
</div>

</div>
<div class="ready-post-section">
<div class="ready-post-sectionin">
<div class="wrapper-in">
<div class="common-wrap">
<div class="readytxt">Ready to post your project today?</div>
<div class="readytxt1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
<div class="postbtn2"><a href="">Post Your Project Today!</a></div>
</div>
</div>
</div>
</div>
<!-- end body wrapper-->
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/accodian.js"></script>
<?php get_footer(); ?>