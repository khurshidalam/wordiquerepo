<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<?php wp_head(); ?>

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,400italic,500,500italic,700,700italic,900,900italic,100' rel='stylesheet' type='text/css'/>
<link href='http://fonts.googleapis.com/css?family=Averia+Serif+Libre:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'/>

<!-- css -->
<link href="<?php bloginfo('template_directory');?>/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_directory');?>/css/jquery-feature.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_directory');?>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_directory');?>/css/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_directory');?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_directory');?>/css/owl.carousel.css" rel="stylesheet">




<!-- js -->
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery-1.9.1.min.js"></script> 
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/owl.carousel.js"></script>
<script src="<?php bloginfo('template_directory');?>/js/jquery-scrolltofixed.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).bind("load", function() {
var owl = $("#owl-demo");
owl.owlCarousel({
navigation : true,
singleItem : true,
transitionStyle : "fade"
});
$("#transitionType").change(function(){
var newValue = $(this).val();
owl.data("owlCarousel").transitionTypes(newValue);
owl.trigger("owl.next");
});
});
</script>
 <script type="text/javascript">
$(function() {
$('#menu-icon').click(function() {
$("#menu-home").slideToggle();
});
});
</script>
<script type="text/javascript">
$(document).ready(function() {
$('.header-wrapper').scrollToFixed();
$('.footer').scrollToFixed( {
bottom: 0,
limit: $('').offset().top
});
var summaries = $('');
summaries.each(function(i) {
var summary = $(summaries[i]);
var next = summaries[i + 1];

summary.scrollToFixed({
marginTop: $('.header-wrapper').outerHeight(true) + 10,
limit: function() {
var limit = 0;
if (next) {
limit = $(next).offset().top - $(this).outerHeight(true) - 10;
} else {
limit = $('').offset().top - $(this).outerHeight(true) - 10;
}
return limit;
},
zIndex: 9999
});
});
});
</script>
<script src="<?php bloginfo('template_directory');?>/js/jquery.dropdownlist.js"></script>

<style>
	.job-manager-form{
		width: 70%;
		padding: 20px;
		margin: 20px 10px;
		float: left;
	}
	.postform, .input-text{
		border: 1px solid #ccc;
		border-radius:2px;
		padding: 5px;
		
	}
	
	.button{
		background: #E3493B;
		
		padding: 10px;
		border-radius: 5px;
		box-shadow: 1px 2px 4px 1px #8F8F8F;
		color: #fff;
	}
	.button:hover{
		
		background: #AD251B;
	}
	#post-4{
		margin: 50px 0 0 0;
	}
</style>
<script type='text/javascript'>
(function (d, t) {
  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
  bh.type = 'text/javascript';
  bh.src = '//www.bugherd.com/sidebarv2.js?apikey=hunnlvtqvi04gip7uua3pw';
  s.parentNode.insertBefore(bh, s);
  })(document, 'script');
</script>
</head>

<body>



<!-- start main-wrapper -->

 <?php  $home_url=get_home_url().'/';
	$curr_url=current_page_url();
	if($curr_url==$home_url)
	 $hd_cls='class="header-wrapper"';
	else
	 $hd_cls='class="scroll-to-fixed-fixed"';
  
 ?> 

<div class="main-wrapper">
<div class="header-line"></div>
<!-- start header-wrapper -->
<div <?php echo $hd_cls; ?> >
<div class="wrapper-in">
<a href="<?php echo home_url('/'); ?>"><div class="logo"></div></a>
<div id="search-box">
<form action="" autocomplete="on">
  <div id="search">
  <input name="search" type="text" placeholder="Search"></div>
  <div id="search_submit">
  <input id="" value="Rechercher" type="submit"></div>
  </form>
  </div>
<div class="sign-up"><a href="<?php echo home_url('/').'wp-login.php?action=register'; ?>">sign up</a></div>
<div class="login"><a href="<?php echo home_url('/').'wp-login.php'; ?>">Login</a></div>
<div class="menu">
<div id="menu-icon">&nbsp; Navigation</div>
<!--<ul id="dd">
<li class="active"><a href="index.html">HOME</a></li>
<li><a href="about-us.html">ABOUT US</a></li>
<li><a href="member-resources.html">MEMBER RESOURCES</a></li>
<li><a href="blog.html">FAQ</a></li>
<li><a href="contact-us.html">CONTACTS</a></li>
</ul>-->
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
<div class="clear"></div>
</div>
</div>

<!-- end header-wrapper -->

