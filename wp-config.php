<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordique');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'b3net');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Gd$Z!lIryP!-!p3xg>Pt`8uu*%+)-/bp4-0DHlVu;5y_o7[f{)B&w ,3FbRW>!f ');
define('SECURE_AUTH_KEY',  'u>41EVIZQaoJ[EXia<HxbCge4(.vxH&ziguD1]gpq=w%F}nZ`Zs^@ik=O;k~idj|');
define('LOGGED_IN_KEY',    '_i4L*YMD/Bz,`+PqMThE|kvd`I?F^!!HVt0TC- B{FUY&|}vN50f*v+X=QYU<UxW');
define('NONCE_KEY',        '=|Ce__bfU}JN/_h9.%6[c06+1JOM$n%8QBYvPG`g5yq3m8dHv!<-$<GfRF:?2cMQ');
define('AUTH_SALT',        'BtjnVFc78Y~N kgHW,gsB}1}dgTf%i(v{[k<uhwn8SntoI86#8n1h+A]+GK9YT-j');
define('SECURE_AUTH_SALT', '}J$HkVn]E9H|*TF8+OG:83kr+bG(ljwe: %G yJ2(~7t8u%ZQex-.57)Uc/&eH82');
define('LOGGED_IN_SALT',   '3NmIi@l08%|B-kISi9O=7d{/c&1iA;w`38},uzOOow)X|{>?;d:+TcJN9{!MG.IS');
define('NONCE_SALT',       'OYbnLyi7FS>J5s%4jwzKJB5ymd%=>D_ s$i]SVeON:3;%8j9a|K;9PpI*d>P*:E|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
